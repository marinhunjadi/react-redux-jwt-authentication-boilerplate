import React from 'react';
import { Route } from 'react-router-dom';
import App from '../components/app';
import RequireAuth from '../components/auth/require_auth';
import Signin from '../components/auth/signin';
import Signout from '../components/auth/signout';
import Signup from '../components/auth/signup';
import Feature from '../components/feature';
import Currencies from '../components/currencies';
import Currency from '../components/currency';
import Welcome from '../components/welcome';

const Routes = () => {
    return (
        <App>
            <Route exact path="/" component={Welcome} />
            <Route exact path="/signin" component={Signin} />
            <Route exact path="/signout" component={Signout} />
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/feature" component={RequireAuth(Feature)} />
			<Route exact path="/currencies" component={Currencies} />
			<Route exact path="/currency/:valuta/:datum_primjene_od/:datum_primjene_do" component={Currency} />
        </App>
    );
};

export default Routes;
