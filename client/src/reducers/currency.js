import {
    FETCH_CURRENCIES,
    FETCH_CURRENCY	
} from '../actions/types';

export const reducer = (state = {}, action) => {

    switch (action.type) {
        case FETCH_CURRENCIES:
            return { ...state, listCurrencies: action.payload}
        case FETCH_CURRENCY:
            return { ...state, listCurrency: action.payload}			
        default:
            return state;
    }
};
