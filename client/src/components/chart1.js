import React, { PureComponent } from 'react';
import { Chart } from 'react-charts';

class Chart1 extends PureComponent {
	/*data() {
		return [
		  {
			label: 'Series 1',
			data: [[0, 1], [1, 2], [2, 4], [3, 2], [4, 7]]
		  },
		  {
			label: 'Series 2',
			data: [[0, 3], [1, 1], [2, 5], [3, 6], [4, 4]]
		  }
		];
	};*/
	
	axes() {
		return [
			{ primary: true, type: 'linear', position: 'bottom' },
			{ type: 'linear', position: 'left' }
		];
	};
	
    render() {
        /*if (!this.props.currencies) {
            return <div>Loading...</div>;
        }*/

        return (
			<div
			  style={{
				width: '400px',
				height: '300px'
			  }}
			>
			  <Chart data={this.props.data} axes={this.axes()} />
			</div>
        );
    }
}

export default Chart1;
