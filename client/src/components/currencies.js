import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Link } from 'react-router-dom';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';

class Currencies extends PureComponent {
    state = {
		dateFrom: new Date(),
		dateTo: new Date(),		
    }

    onChangeFrom = dateFrom => this.setState({ dateFrom })
    onChangeTo = dateTo => this.setState({ dateTo })	
	
    componentWillMount() {
        this.props.fetchCurrencies();
    }

    renderCurrency() {
        return this.props.currencies.map(currency => {            
			return <tr key={currency.valuta}><td><Link to={{pathname: `/currency/${currency.valuta}/${this.formatDate(this.state.dateFrom)}/${this.formatDate(this.state.dateTo)}`}}>{currency.valuta}</Link></td><td>{currency.drzava}</td><td>{currency.jedinica}</td><td>{currency.kupovni_tecaj}</td><td>{currency.srednji_tecaj}</td><td>{currency.prodajni_tecaj}</td></tr>;
        })
    }

	formatDate(date) {
		//var date = new Date();
		var dd = String(date.getDate()).padStart(2, '0');
		var mm = String(date.getMonth() + 1).padStart(2, '0'); //January is 0!
		var yyyy = date.getFullYear();

		date = yyyy + '-' + mm + '-' + dd;	
		return date;
	}

    render() {
        if (!this.props.currencies) {
            return <div>Loading...</div>;
        }

        return (
            <div>				
                <h4>Currencies</h4>
				<table>
				<tbody>
				<tr><td>
				<Calendar
				  onChange={this.onChangeFrom}
				  value={this.state.dateFrom}
				/>
				</td><td>
				<Calendar
				  onChange={this.onChangeTo}
				  value={this.state.dateTo}
				/>
				</td></tr>
				</tbody>
				</table>
				<p></p>
                <table>
				<tbody>
				<tr><td>Valuta</td><td>Država</td><td>Jedinica</td><td>Kupovni tečaj</td><td>Srednji tečaj</td><td>Prodajni tečaj</td></tr>
                    {this.renderCurrency()}
				</tbody>
                </table>
            </div>
			
        );
    }
	
	
}

const mapStateToProps = (state) => {
    return { currencies: state.currencies.listCurrencies }
}

export default connect(mapStateToProps, actions)(Currencies);
