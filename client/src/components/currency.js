import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { Link } from 'react-router-dom';
import Chart1 from './chart1';

class Currency extends PureComponent {
	
    componentWillMount() {
		this.props.fetchCurrencies();
        this.props.fetchCurrency(this.props.match.params.valuta, this.props.match.params.datum_primjene_od, this.props.match.params.datum_primjene_do);
    }

    renderCurrency() {
        return this.props.currencies.map(currency => {
            //return <li key={currency.sifra_valute}><a href="/currency?valuta=" + {currency.valuta} onClick={() => this.setChart(currency.valuta)}>{currency.valuta}</a></li>;
			return <li key={currency.valuta}><Link onClick={this.componentWillMount} to={{pathname: `/currency/${currency.valuta}/${this.props.match.params.datum_primjene_od}/${this.props.match.params.datum_primjene_do}`}}>{currency.valuta}</Link></li>;
        })
    }
	
	data() {
		if (this.props.currency) {
			var values = [];
			var items = this.props.currency;
			for (var i = 0; i < items.length; i++) {
				values.push([i, parseFloat(items[i].srednji_tecaj)]);
			}

			return [
			  {
				label: 'Series 1',
				data: values
			  }
			];
		} else {
			return [
			  {
				label: 'Series 1',
				data: []
			  }
			];		
		}
	}

    render() {
        if (!this.props.currency) {
            return <div>Loading...</div>;
        }

        return (
            <div>				
                <h4>Currency {this.props.currency.valuta}</h4>				
				<ul>
				{this.renderCurrency()}
				</ul>
				<Chart1 data={this.data()} />
            </div>
			
        );
    }
	
	
}

const mapStateToProps = (state) => {
    return { currencies: state.currencies.listCurrencies, currency: state.currencies.listCurrency }
}

export default connect(mapStateToProps, actions)(Currency);
