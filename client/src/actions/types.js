export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';
export const AUTH_ERROR = 'auth_error';
export const FETCH_FEATURE = 'fetch_feature';
export const FETCH_CURRENCIES = 'fetch_currencies';
export const FETCH_CURRENCY = 'fetch_currency';
