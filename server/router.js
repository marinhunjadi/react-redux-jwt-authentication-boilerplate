const Authentication = require('./controllers/authentication');
const passwordService = require('./services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignin = passport.authenticate('local', { session: false });

const request = require('request');

module.exports = (app) => {

    app.post('/signin', requireSignin, Authentication.signin);
    app.post('/signup', Authentication.signup);

    app.get('/', requireAuth, (req, res, next) => {
        res.send(['water', 'phone', 'paper']);
    });
	
	app.get('/tecajn/v2', function(req, res){ 

		request('http://api.hnb.hr/tecajn/v2', function (error, response, body) {
			  console.log('error:', error); // Print the error if one occurred and handle it
			  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
			  res.send(body)
		});

	});

	app.get('/tecajn/v2/:valuta/:datum_primjene_od/:datum_primjene_do', function(req, res){ 
	   /*var valuta = req.params.valuta,
		   datum_primjene_od = req.params.datum_primjene_od,
		   datum_primjene_do = req.params.datum_primjene_do;*/
		request('http://api.hnb.hr/tecajn/v2?datum-primjene-od=' + req.params.datum_primjene_od + '&datum-primjene-do=' + req.params.datum_primjene_do, function (error, response, body) {
			  console.log('error:', error); // Print the error if one occurred and handle it
			  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
			  //console.log(body);
			  body = JSON.parse(body).filter(b => b.valuta === req.params.valuta);
			  res.send(body)
		});

	});	
};
